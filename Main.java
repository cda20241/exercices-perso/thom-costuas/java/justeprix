/**
 * The Main class serves as the entry point for the "Juste Prix" game application.
 * It handles user input, game flow, and interaction with the JustePrix game logic.
 * The game provides options to start a new round, modify game settings,
 * view the leaderboard, or exit the application.
 */


import java.util.Scanner;
import java.util.Random;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    /**
     * The main method initializes the JustePrix game instance, displays the welcome message,
     * and provides a menu for the user to interact with the game.
     *
     * @param args The command-line arguments (not used in this application).
     */
    public static void main(String[] args) {
        JustePrix newGame = new JustePrix();

        boolean exit = false;
        System.out.println("Bienvenue dans le jeu du Juste Prix ! \n");
        System.out.println("Voici nos 10 derniers gagnants : \n");
        newGame.getResults();

        while (!exit) {
            System.out.println("\n");
            System.out.println("1. Lancer une partie");
            System.out.println("2. Modifier les options");
            System.out.println("3. Consulter le tableau des scores");
            System.out.println("4. Quitter la partie");
            Scanner scanner = new Scanner(System.in);
            System.out.print("\nChoisissez une option valide : ");
            String selectedOption = scanner.next();

            if (selectedOption.equals("1")) {
                clearScreen();
                boolean ok = false;
                while (!ok) {
                    System.out.print("\nChoisissez votre nom (la partie démarrera après la sélection du nom) : ");
                    String name = scanner.next();

                    if (name.equals("exit")) {
                        System.out.println("Ce nom est invalide ! ");
                    } else {
                        ok = true;
                        newGame.setPlayerName(name);
                    }
                }

                clearScreen();
                newGame.generateRandomValue();
                newGame.startChrono();
                boolean win = false;

                while (!win) {
                    clearScreen();
                    System.out.print("Votre choix ---> ");
                    int tentative = scanner.nextInt();
                    win = newGame.plusOuMoins(tentative);
                }

                newGame.addResults(newGame.getPlayerName() + " : " + newGame.getScore() + "\n");
                System.out.println(newGame.getPlayerName() + " : " + newGame.getScore());

            } else if (selectedOption.equals("2")) {
                clearScreen();
                boolean exitOptions = false;
                System.out.println("Que voulez-vous modifier ? \n");

                while (!exitOptions) {
                    System.out.println("1. Modifier valeur maximale");
                    System.out.println("2. Modifier valeur minimale");
                    System.out.println("3. Revenir au menu");
                    System.out.print("\nChoisissez une option valide : ");
                    String option = scanner.next();

                    if (option.equals("1")) {
                        System.out.print("\nNouvelle valeur maximale : ");
                        int val = scanner.nextInt();
                        newGame.setValeurMax(val);
                        exitOptions = true;

                    } else if (option.equals("2")) {
                        System.out.print("\nNouvelle valeur minimale : ");
                        int val = scanner.nextInt();
                        newGame.setValeurMin(val);
                        exitOptions = true;

                    } else if (option.equals("3")) {
                        exitOptions = true;
                        clearScreen();

                    } else {
                        System.out.println("\nChoix invalide ! \n");
                    }
                }

            } else if (selectedOption.equals("3")) {
                clearScreen();
                boolean exitResultat = false;
                System.out.println("\n\nMenu de recherche des résultats: \n");

                List<String> data = newGame.searchResults();

                while (!exitResultat) {
                    System.out.print("\nVotre recherche (tapez 'exit' pour quitter) : ");
                    String recherche = scanner.next();
                    if (recherche.toLowerCase().equals("exit")) {
                        exitResultat = true;
                        clearScreen();
                    } else {
                        List<String> resultatsTrouves = new ArrayList<>();
                        for (String ligne : data) {
                            if (ligne.toLowerCase().contains(recherche.toLowerCase())) {
                                resultatsTrouves.add(ligne);
                            }
                        }

                        if (!resultatsTrouves.isEmpty()) {
                            System.out.println();
                            for (String resultat : resultatsTrouves) {
                                System.out.println(resultat);
                            }
                        } else {
                            System.out.println("Aucun résultat trouvé.");
                        }
                    }
                }

            } else if (selectedOption.equals("4")) {
                System.out.println("\nAu revoir");
                exit = true;

            } else {
                System.out.println("\nChoix invalide ! \n");
            }
        }
    }

    /**
     * Clears the console screen by printing newline characters.
     */
    private static void clearScreen() {
        System.out.println("\n".repeat(7));
    }
}

/**
 * The JustePrix class encapsulates the game logic for the "Juste Prix" game.
 * It includes methods for setting game options, generating random numbers,
 * managing player information, recording results, and searching the results.
 */
class JustePrix {

    private int valeurMin;
    private int valeurMax;
    private String playerName;
    private Integer randomValue;
    private long chronoDebut;
    private double score;

    /**
     * Constructs a new JustePrix game instance with default values.
     */
    public JustePrix() {
        this.valeurMin = 0;
        this.valeurMax = 100;
        this.playerName = "";
        this.randomValue = null;
        this.chronoDebut = 0;
        this.score = 0;
    }

    /**
     * Sets the minimum value for the random number within the valid range.
     *
     * @param newMin The new minimum value.
     * @throws IllegalArgumentException if the new minimum value is greater than or equal to the current maximum value.
     */
    public void setValeurMin(int newMin) {
        if (newMin < this.valeurMax) {
            this.valeurMin = newMin;
            System.out.println("\nNouvelle valeur minimale modifiée avec succès\n");
        } else {
            throw new IllegalArgumentException("InvalidMinValue, value is Superior to max value");
        }
    }

    /**
     * Sets the maximum value for the random number within the valid range.
     *
     * @param newMax The new maximum value.
     * @throws IllegalArgumentException if the new maximum value is less than or equal to the current minimum value.
     */
    public void setValeurMax(int newMax) {
        if (newMax > this.valeurMin) {
            this.valeurMax = newMax;
            System.out.println("\nNouvelle valeur maximale modifiée avec succès\n");
        } else {
            throw new IllegalArgumentException("InvalidMaxValue, value is Inferior to max value");
        }
    }

    /**
     * Gets the current minimum value for the random number.
     *
     * @return The current minimum value.
     */
    public int getMinvaleur() {
        return this.valeurMin;
    }

    /**
     * Gets the current maximum value for the random number.
     *
     * @return The current maximum value.
     */
    public int getMaxvaleur() {
        return this.valeurMax;
    }

    /**
     * Generates a new random number within the current valid range.
     */
    public void generateRandomValue() {
        this.randomValue = new Random().nextInt(this.valeurMax - this.valeurMin + 1) + this.valeurMin;
    }

    /**
     * Gets the last generated random number.
     *
     * @return The last generated random number.
     */
    public Integer getRandomValue() {
        return this.randomValue;
    }

    /**
     * Sets the player name for the current game.
     *
     * @param newPlayerName The new player name.
     */
    public void setPlayerName(String newPlayerName) {
        this.playerName = newPlayerName;
    }

    /**
     * Gets the current player name for the game.
     *
     * @return The current player name.
     */
    public String getPlayerName() {
        return this.playerName;
    }

    /**
     * Records the game result, including the player name and score, to a results file.
     *
     * @param result The result to be recorded.
     */
    public void addResults(String result) {
        try (FileWriter writer = new FileWriter("Resultats.txt", true)) {
            writer.write(result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Displays the last 10 game results from the results file.
     */
    public void getResults() {
        try (BufferedReader reader = new BufferedReader(new FileReader("Resultats.txt"))) {
            List<String> data = new ArrayList<>();
            String line;
            while ((line = reader.readLine()) != null) {
                data.add(line);
            }
            for (int i = Math.max(0, data.size() - 10); i < data.size(); i++) {
                System.out.println(data.get(i).trim());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Searches the game results file for specific keywords provided by the user.
     *
     * @return A list of results matching the user's search criteria.
     */
    public List<String> searchResults() {
        List<String> data = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader("Resultats.txt"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                data.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    /**
     * Checks if the player's guess is higher, lower, or equal to the random number.
     * Updates the game state accordingly.
     *
     * @param value The player's guess.
     * @return True if the player guessed correctly, false otherwise.
     */
    public boolean plusOuMoins(Integer value) {
        if (value == null) {
            return false;
        }

        if (value > this.randomValue) {
            System.out.println("C'est moins \n");
            return false;

        } else if (value < this.randomValue) {
            System.out.println("C'est plus \n");
            return false;

        } else {
            this.winner();
            System.out.println("C'est gagné \n");
            return true;
        }
    }

    /**
     * Records the end time and calculates the player's score based on the elapsed time.
     */
    public void startChrono() {
        this.chronoDebut = System.currentTimeMillis();
    }

    /**
     * Calculates the player's score based on the elapsed time.
     */
    public void winner() {
        long end = System.currentTimeMillis();
        this.score = (double) (end - this.chronoDebut) / 1000;
    }

    /**
     * Gets the player's current score.
     *
     * @return The player's score.
     */
    public double getScore() {
        return this.score;
    }
}
